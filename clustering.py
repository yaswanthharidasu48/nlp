from sklearn_extra.cluster import KMedoids
from preprocessing import Preprocess
from sklearn.metrics import silhouette_score
import numpy as np


class Cluster:
    def __init__(self, preprocess):
        self.medoids = []
        self.sentence_vectors = preprocess.sentence_vectors

    def generate_clusters(self, no_of_clusters):
        medoid_generator = KMedoids(metric="manhattan", n_clusters=no_of_clusters, init="heuristic", max_iter=100).fit(self.sentence_vectors)
        self.medoids = medoid_generator.cluster_centers_
        self.medoids = self.medoids.tolist()
        return self.medoids

    def silhouette_score(self):
        for n in range(2, len(self.sentence_vectors)):
            # Initialize the clusterer with n_clusters value and a random generator seed of 10 for reproducibility.
            clusterer = KMedoids(metric="manhattan", n_clusters=n, init="heuristic", max_iter=100)
            cluster_labels = clusterer.fit_predict(self.sentence_vectors)
            # The silhouette_score gives the average value for all the samples.
            silhouette_avg = silhouette_score(self.sentence_vectors, cluster_labels)
            print("For n_clusters = ", n, "The average silhouette_score is : ", silhouette_avg)
