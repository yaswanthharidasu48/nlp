from individual import Individual
import numpy as np
import math


class Problem:
    def __init__(self, sentence_vectors, medoids, no_of_sentences, summary_length, expand=True):
        self.expand = expand
        self.no_of_sentences = no_of_sentences
        self.sentence_vectors = sentence_vectors
        self.medoids = medoids
        self.summary_length = summary_length

    def discretization(self, x):
        sigmoid_value = 1 / (1 + math.exp(-x))
        if x < sigmoid_value:
            return 1
        else:
            return 0

    def generate_individual(self):
        individual = Individual()
        genes = list(map(self.discretization, np.random.uniform(0, 1, self.no_of_sentences)))
        while genes.count(1) <= self.summary_length or genes.count(1) == self.no_of_sentences:
            genes = list(map(self.discretization, np.random.uniform(0, 1, self.no_of_sentences)))
        individual.features = genes
        return individual

    def cosine_similarity(self, x, y):
        cos = np.dot(x, y) / (np.sqrt(np.dot(x, x)) * np.sqrt(np.dot(y, y)))
        return cos

    def calculate_coverage(self, sentence_vectors, individual, medoids):
        coverage = 0
        for gene_index in range(0, len(individual)):
            # If that sentence is included in summary
            if individual[gene_index] == 1:
                sum = 0
                # Calculate sum of cosine similarities between that sentence and all medoids
                for medoid in medoids:
                    sum += self.cosine_similarity(sentence_vectors[gene_index], medoid)
                coverage += sum
        return coverage

    def calculate_redundancy(self, sentence_vectors, individual):
        redundancy = 0
        for gene_index in range(0, len(individual)):
            if individual[gene_index] == 1:
                sum = 0
                for i in range(gene_index + 1, len(individual)):
                    if individual[i] == 1:
                        sum += self.cosine_similarity(sentence_vectors[gene_index], sentence_vectors[i])
                redundancy += sum
        return redundancy

    def calculate_objectives(self, individual):
        individual.objectives = [self.calculate_coverage(self.sentence_vectors, individual.features, self.medoids),
                                 self.calculate_redundancy(self.sentence_vectors, individual.features)]
