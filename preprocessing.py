from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize, sent_tokenize
import regex as re
import string
import nltk
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer


# nltk.download('wordnet')
# nltk.download('punkt')


class Preprocess:
    def __init__(self):
        self.sentences = []
        self.sentence_vectors = []
        self.sentence_tokens = []
        self.word_tokens = []
        self.text = ''
        self.tf_idf_values = dict()

    def read_file(self):
        # file opening
        # fname = input("Enter file name: ")
        fname = "sample1.txt"
        fh = open(fname, 'r', encoding="utf8")
        doc = fh.read()
        # print(doc)
        # removing extra spaces
        self.text = re.sub("\s+", " ", doc)

    def generate_sentence_tokens(self):
        self.sentences = sent_tokenize(self.text)
        self.sentence_tokens = sent_tokenize(self.text)
        for i in range(len(self.sentence_tokens)):
            self.sentence_tokens[i] = self.sentence_tokens[i].lower()
            self.sentence_tokens[i] = re.sub(r'\W', ' ', self.sentence_tokens[i])
            self.sentence_tokens[i] = re.sub(r'\s+', ' ', self.sentence_tokens[i])
        # print("Sentences: \n", self.sentences)
        return self.sentence_tokens

    def generate_word_tokens(self):
        # removing punctuations
        punctuation_free_text = "".join([i.lower() for i in self.text if i not in string.punctuation])
        # print(punctuation_free_text)

        self.word_tokens = word_tokenize(punctuation_free_text)
        # print('Words: \n', self.word_tokens)

        # stop word removal
        self.word_tokens = [i for i in self.word_tokens if i not in stopwords.words('english')]
        # print('Words: \n', self.word_tokens)
        # print(len(self.word_tokens))

        # Lemmatization
        lemma = WordNetLemmatizer()
        self.word_tokens = [lemma.lemmatize(word, pos="v") for word in self.word_tokens]
        # print('Words after lemmatization: \n', self.word_tokens)
        self.word_tokens = list(set(self.word_tokens))
        self.word_tokens.sort()
        return self.word_tokens

    def generate_sentence_vectors(self):
        word_tokens = self.word_tokens
        print("Generating sentence vectors...")
        # Finding TF-IDF values
        vectorizer = TfidfVectorizer(stop_words=stopwords.words("english"), vocabulary=word_tokens)
        tf_idf = vectorizer.fit_transform([self.text])

        # Unique words from vectorizer
        # unique_words = vectorizer.get_feature_names()

        # Storing tf-idf values in a dataframe
        df = pd.DataFrame(tf_idf[0].T.todense(), index=word_tokens, columns=["TF-IDF"])
        df = df.sort_values('TF-IDF', ascending=False)
        # print(df.head(25))

        # Creating a dict for tf-idf values
        for word in word_tokens:
            self.tf_idf_values[word] = df.loc[word]["TF-IDF"]
        # print("tf-idf values:\n", self.tf_idf_values)

        for sentence in self.sentence_tokens:
            sent_vector = []
            for word in word_tokens:
                if word in sentence:
                    sent_vector.append(self.tf_idf_values[word])
                else:
                    sent_vector.append(0)
            self.sentence_vectors.append(sent_vector)
        return self.sentence_vectors
