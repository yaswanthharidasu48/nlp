from preprocessing import Preprocess
from clustering import Cluster
from evolution import Evolution
from problem import Problem
import matplotlib.pyplot as plt

preprocess = Preprocess()
preprocess.read_file()
preprocess.generate_sentence_tokens()
preprocess.generate_word_tokens()
preprocess.generate_sentence_vectors()

# print(preprocess.sentence_tokens)
# print(preprocess.word_tokens)
# print(preprocess.sentence_vectors)

cluster = Cluster(preprocess)
# cluster.silhouette_score()
cluster.generate_clusters(no_of_clusters=5)
# print("Medoids:\n", cluster.medoids)

problem = Problem(sentence_vectors=preprocess.sentence_vectors, medoids=cluster.medoids,
                  no_of_sentences=len(preprocess.sentences), summary_length=10)
evolution = Evolution(problem)
# pareto_front = evolution.evolve()

print("No.of sentences:", len(preprocess.sentence_tokens))
# print("Length of pareto front:", len(pareto_front))
# for i in range(0, len(pareto_front)):
#     print(i, pareto_front[i].features)


pareto_front = []
for i in range(10):
    solutions = evolution.evolve()
    print("length", len(solutions))
    pareto_front.extend(solutions)

print("Length of pareto front:", len(pareto_front))
for i in range(0, len(pareto_front)):
    print(i, pareto_front[i].features)


pareto_front_len = len(pareto_front)
if pareto_front_len == 1:
    for gene in range(len(preprocess.sentences)):
        if pareto_front[0].features[gene] == 1:
            print(preprocess.sentences[gene])
else:
    for gene in range(len(preprocess.sentences)):
        no_of_ones = 0
        for individual in pareto_front:
            if individual.features[gene] == 1:
                no_of_ones += 1
        if no_of_ones >= pareto_front_len//4:
            print(preprocess.sentences[gene])

